@extends('layouts.auth')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1 class="text-center" style="color: white">Cuộc thi tìm hiểu về Chủ Tịch Hồ Chí Minh</h1>
            <h3 class="text-center" style="color: white">Bạn đăng nhập để tham dự cuộc thi</h3>
            <br />
            <div class="panel panel-default">
                <div class="panel-heading">Đăng nhập</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Có lỗi xảy ra!</strong> Vui lòng kiểm tra thông tin đăng nhập:
                            <br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form-horizontal"
                          role="form"
                          method="POST"
                          action="{{ url('login') }}">
                        <input type="hidden"
                               name="_token"
                               value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                <input type="email"
                                       class="form-control"
                                       name="email"
                                       value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Mật khẩu</label>

                            <div class="col-md-6">
                                <input type="password"
                                       class="form-control"
                                       name="password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <label>
                                    <input type="checkbox"
                                           name="remember">Ghi nhớ đăng nhập?
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit"
                                        class="btn btn-primary">
                                    Đăng nhập
                                </button>
                                <a href="{{ route('auth.register') }}"
                                        class="btn btn-default">
                                    Đăng ký tài khoản
                                </a>
                                <br>
                                <a href="{{ route('auth.password.reset') }}">Quên mật khẩu</a>
{{--                                <br>--}}
{{--                                <br>--}}
{{--                                Or login with:--}}
{{--                                <br>--}}
{{--                                <a href="{{ route('oauth2google') }}"--}}
{{--                                        class="btn btn-info">--}}
{{--                                    Google--}}
{{--                                </a>--}}
{{--                                <a href="{{ route('oauth2facebook') }}"--}}
{{--                                        class="btn btn-info">--}}
{{--                                    Facebook--}}
{{--                                </a>--}}
{{--                                <a href="{{ route('oauth2github') }}"--}}
{{--                                        class="btn btn-info">--}}
{{--                                    GitHub--}}
{{--                                </a>--}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="text-center" style="color: white">Đơn vị tổ chức  <a href="http://ldldq02hcm.org.vn/">Liên Đoàn Lao Động Quận 2, Tp. Hồ Chí Minh</a></div>
        </div>
    </div>
@endsection
